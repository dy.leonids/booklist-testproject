import React, { Component }  from 'react';
import { connect } from 'react-redux';
import { getBooksInfomation } from '../../actions/bookActions'
import BookListView from '../../components/booksListView';

    class BookList extends Component {

        constructor(props) {
            super(props)
            this.state = {
            }
        }

    componentWillMount() {
        this.props.dispatch(getBooksInfomation());
      }

    render(){
       

        return(
            <div>  
                
                <BookListView 
                    booksData = {this.props.booksData}
                />

            </div>
    )}
}

const mapStateToProps = (state) => {
    return {
        booksData : state.bookStore.booksData,
    }
  }
  
  export default connect(mapStateToProps)(BookList);