import React, { Component }  from 'react';
import { connect } from 'react-redux';
import BookDetailVeiw from '../../components/bookDetailsView';
import {Link} from 'react-router-dom';

    class BookDetails extends Component {

        constructor(props) {
            super(props)
            this.state = {
                editBookData : ""
            }
          }

          componentWillMount(){
            
            if( this.props.location.state === undefined){
                this.props.history.push("/");
            }else{
        
            const { editBookData } = this.props.location.state;
            this.setState({
                editBookData : editBookData
              });
            }
          }

         

        

    render(){

        return(
            <div style={{ padding: "5%"}}>  
               <Link to="/"><button style = {{ padding :"5px",borderRadius: "12px", fontSize:"20px"}}> Back</button></Link>
                <BookDetailVeiw
                  editBookData = { this.state.editBookData}
                />
            </div>
    )}
}

const mapStateToProps = (state) => {
    return {
       
    }
  }
  
export default connect(mapStateToProps)(BookDetails);