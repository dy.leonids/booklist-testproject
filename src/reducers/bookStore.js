const initialState = {
  
  }
  
  const bookStore = (state = initialState , action) =>{
    switch (action.type) {
        
        case 'SET_BOOK_INFORMATION' :
          return {
            ...state,
            booksData: action.response,
          }

        default:
            return state;
          }
  };

  export default bookStore;
  