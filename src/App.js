import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import BookList from './pages/bookStore/bookList';
import BookDetails from './pages/bookStore/bookDetails';
import './App.css';
class App extends Component {
   render() {
       return (
        <Router>
          <Switch>
              <Route exact path='/' component={BookList} />
              <Route exact path='/book-details' component={BookDetails} />
          </Switch>
        </Router>
       );
   }
}
export default App;