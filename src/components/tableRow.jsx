// TableRow.js

import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class TableRow extends Component {
  render() {
    return (
        <tr>
          <td>
            <Link to = {{      
            pathname: '/book-details',
            state: {editBookData : this.props.obj.volumeInfo }
            }}> {this.props.obj.volumeInfo.title}
            </Link>
          </td>
        </tr>
    );
  }
}

export default TableRow;