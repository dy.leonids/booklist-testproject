import React, { Component }  from 'react';
import TableRow from './tableRow.jsx';
import './style.css'
export default class BookListView extends Component {
    
    constructor(props) {
        super(props)
        this.state = {
          
        }  
      }
      tabRow(){
        if(this.props.booksData && this.props.booksData.items instanceof Array){
          return this.props.booksData.items.map(function(object, i){
              return <TableRow obj={object} key={i} />;
          })
        }
      }

      

    render(){
        return(
            <div style={{ padding :"5%"}}>
             <table id="customers">
                <tr>
                    <th>Book's titles</th>
                </tr>
                <tbody>
                    {this.tabRow()}
                </tbody>
                </table>
            </div>
    )}
}

