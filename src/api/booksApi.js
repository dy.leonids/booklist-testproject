export const getBooksInfomationApi =( )=> {

    const headers = Object.assign({'Content-Type': 'application/json'});
    const request= new Request( 'https://www.googleapis.com/books/v1/volumes?filter=free-ebooks&q=a', {
        method : "GET",
        headers : headers
    });
    
    return  fetch(request).then(response => {
    return  response.json().then(bookRes => {
        return bookRes;
    });
    }).catch(error => {return error;});
}