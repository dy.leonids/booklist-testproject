import { getBooksInfomationApi } from '../api/booksApi';

export const setBooksInfomation = (response) => {
	return {
	type: "SET_BOOK_INFORMATION",
	response
	}
}

export const getBooksInfomation = () => {
	return dispatch => {     
        getBooksInfomationApi( ).then((result) => {
             dispatch(setBooksInfomation(result));
        });
    }
}
